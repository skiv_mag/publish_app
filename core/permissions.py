from rest_framework import permissions
from rest_framework.compat import is_authenticated


class IsEditor(permissions.BasePermission):
    def has_permission(self, request, view):
        is_auth = request.user and is_authenticated(request.user)
        return is_auth and request.user.has_perm('core.change_published_status')
