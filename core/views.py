from core.models import Post
from core.permissions import IsEditor
from core.serializers import AuthTokenEmailSerializer, UserSerializer, PostsSerializer, UserProfileSerializer, \
    PublishPostsSerializer
from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.filters import SearchFilter
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView, UpdateAPIView
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

User = get_user_model()


class AuthMixin:
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)


class PostSearchMixin:
    filter_backends = (SearchFilter,)
    search_fields = ('title', 'body')


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 16
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CreateUserView(CreateAPIView):
    model = User
    permission_classes = [
        permissions.AllowAny  # Or anon users can't register
    ]
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # as we need to rewrite returned data and drf have no hooks for this - we rewrite all method
        # maybe can be done easier
        return_data = {'token': serializer.instance.auth_token.key}
        return Response(return_data, status=status.HTTP_201_CREATED, headers=headers)


class LoginView(ObtainAuthToken):
    serializer_class = AuthTokenEmailSerializer


class CurrentUserView(AuthMixin, APIView):
    def get(self, request):
        serializer = UserProfileSerializer(request.user)
        return Response(serializer.data)


# authentificated users(writers and editors, admins, etc) can create and see all posts)
class PostListCreateView(AuthMixin, PostSearchMixin, ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostsSerializer
    pagination_class = StandardResultsSetPagination


class PublishedPostListView(PostSearchMixin, ListAPIView):
    queryset = Post.objects.filter(is_published=True).all()
    serializer_class = PostsSerializer
    pagination_class = StandardResultsSetPagination


class PublishPostView(UpdateAPIView):
    queryset = Post.objects.filter(is_published=False).all()
    serializer_class = PublishPostsSerializer
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsEditor,)
