from core.models import Post
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    group = serializers.ReadOnlyField()

    # validate group manually as we can't do it automatically
    def validate(self, attrs):
        group_name = self.initial_data.get('group')
        if not group_name:
            raise serializers.ValidationError({"group": "Need group for registration"})
        try:
            group = Group.objects.get(name=group_name)
        except:
            raise serializers.ValidationError({"group": "Define correct group for registration"})
        return super().validate(attrs)

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()

        # set user group on registration as we can't do it automatically
        group_name = self.initial_data.get('group')
        group = Group.objects.get(name=group_name)
        user.groups.add(group)

        return user

    class Meta:
        model = User


# rewrite basic login serializer, as we don't use username anymore
class AuthTokenEmailSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("Email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})


    # rewrite login validation becouse remove username
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(email=email, password=password)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email',)


class PostsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post


class PublishPostsSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        # editor can also have possibility to edit title, body
        # so we make every update on post - partial
        kwargs['partial'] = True
        super().__init__(*args, **kwargs)

    class Meta:
        model = Post
