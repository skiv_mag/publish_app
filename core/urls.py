from core.views import LoginView, CreateUserView, PostListCreateView, CurrentUserView, PublishedPostListView, \
    PublishPostView
from django.conf.urls import url

urlpatterns = [
    url(r'^register', CreateUserView.as_view()),
    url(r'^login', LoginView.as_view()),
    url(r'^me', CurrentUserView.as_view()),
    url(r'^posts$', PostListCreateView.as_view()),
    url(r'^posts/publish/(?P<pk>[0-9]+)', PublishPostView.as_view()),
    url(r'^published_posts', PublishedPostListView.as_view()),

]
