
## Start project from the scratch:

1. Install requirements: pip install -r requirements.txt
2. Migrate database: python manage.py migrate
2. Load fixtures: python manage.py loaddata core/fixtures/initial.json

## URL description:

Notice:
    In urls with token we need to add token to request header in format:
    
    Authorization: Token 47907b4260c644e02f1cbc43a28e1aed379a8ef5

#### Registration
URL: api/register/

Type: POST

Headers: Content-Type: application/json
Example request:
```json
    {
        "email":"a23psdsa@gmail.com",
        "password":"start123",
        "group":"writer"
    }
```
Returns:
```json
    {
      "token": "490909c4d766c034437ec949b06aac5f6f46e82e"
    }
```
Need token: No

Notices: Correct groups are 'writer' and 'editor'
    
#### Login
URL: api/login/

Type: POST

Headers: Content-Type: application/json

Example request:
```json
    {
        "email":"a23psdsa@gmail.com",
        "password":"start123",
    }
```
Need token: No

Returns:
```json
    {
      "token": "490909c4d766c034437ec949b06aac5f6f46e82e"
    }
```    
#### Post creation
URL: api/posts

Type: POST

Headers: Content-Type: application/json

Example request:
```json
    {
        "title":"Hello",
        "body":"Blablbla",
    }
```
Need token: Yes

Returns:
```json
    {
      "id": 2,
      "title": "Hello",
      "body": "Blablabla",
      "is_published": false
    }
```
#### Current user profile
URL: api/me

Type: GET

Headers: No

Example request: No

Need token: Yes

Returns:
```json
    {
      "id": 2,
      "email": "a23pa@gmail.com"
    }
```
#### Publish post
URL: /api/posts/publish/{POST_ID}

Type: PUT

Headers: Content-Type: application/json

Example request: 
```json
    {
        "title":"Hello",
        "body":"Blablabla",
        "is_published":"false"
    }
```
Need token: Yes(accepts only editor token)

Returns:
```json
    {
	"title":"Hello",
	"body":"Blablabla",
	"is_published":"true"
}
```
Notices: When we've already publish post - it will not be shown in all_posts_list
and can't be published again

#### Get all posts
URL: api/posts

Type: GET

Headers: No

Example request: No

Need token: No

Returns:
```json
    {
      "count": 2,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 1,
          "title": "Hello",
          "body": "Blablabla",
          "is_published": true
        },
        {
          "id": 2,
          "title": "Hello",
          "body": "Blablabla",
          "is_published": false
        }
      ]
    }
```   
Filters:

+ page - can set number of page
+ page_size - can set number of posts per page(default - 16)
+ search - searches post by title or body
    
#### Get published posts
URL: api/posts

Do all the same as previous urls, but don't need token and shows posts with
published_status = True
